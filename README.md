# Rogue-Like-Tower-Def

This is a demo project to be used for the SoftUni course on Introduction to Game Development with Unreal Engine.

## Setup

If you want to access the code via Git:
1. Register an account with GitLab
2. Setup your SSH keys by following [GitLab's instructions](https://docs.gitlab.com/ee/user/ssh.html)
3. Ensure you have [Git](https://gitforwindows.org/) and [Git LFS](https://git-lfs.com/) installed.

If you just want to download the code:
1. Click on the "Code" button in the repo and then download the entire project as .zip.

## To run the game

1. Have at least Unreal Engine 5.2.1 installed.
2. Double-click the `RogueLikeTowerDef.uproject` file in the root of the repository.
